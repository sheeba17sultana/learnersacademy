package com.la.controller;

import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.PATCH;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import com.la.service.StudentService;
import com.la.service.impl.StudentServiceImpl;
import com.model.pojo.Student;

@Path("/student")
public class StudentController {
	StudentService service = new StudentServiceImpl();
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public List<Student> getAllStudents() {
		// Presentation Layer, Entry point of the application with URI "/student"
		return service.getAllStudentList();
	}
	
	@POST
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Student createStudent(Student student) {
		// Presentation Layer to create the student
		return service.createStudent(student);
	}
	
	@GET
	@Path("/{id}")
	@Produces(MediaType.APPLICATION_JSON)
	public Student getStudentById(@PathParam("id")int id) {
		// Presentation Layer to get the student by the id entered in the URI
		return service.getStudentById(id);
	}
	
	@PATCH
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Student updateStudent(Student student) {
		// Presentation Layer to update the student
		return service.updateStudent(student);
	}
	
	@DELETE
	@Path("/{id}")
	public void removeStudent(@PathParam("id")int id) {
		service.removeStudent(id);
	}
}
