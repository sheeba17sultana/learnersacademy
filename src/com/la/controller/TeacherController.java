package com.la.controller;

import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.PATCH;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import com.la.service.TeacherService;
import com.la.service.impl.TeacherServiceImpl;
import com.model.pojo.Student;
import com.model.pojo.Subject;
import com.model.pojo.Teacher;

@Path("/teacher")
public class TeacherController {
	
	TeacherService service = new TeacherServiceImpl();
	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public List<Teacher> getAllTeachers() {
		return service.getAllTeacherList();
	}

	@POST
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Teacher createTeacher(Teacher teacher) {
		return service.createTeacher(teacher);
	}

	@GET
	@Path("/{id}")
	public Teacher getTeacherById(@PathParam("id") int id) {
		return service.getTeacherById(id);
	}

	@PATCH
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Teacher updateTeacher(Teacher teacher) {
		return service.updateTeacher(teacher);
	}

	@PATCH
	@Path("/subject")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public List<Subject> updateSubject(Teacher teacher){
		return service.updateSubject(teacher);
	}
	@PATCH
	@Path("/student")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public List<Student> updateStudent(Teacher teacher){
		return service.updateStudent(teacher);
	}
	
	@DELETE
	@Path("/{id}")
	public void removeTeacher(@PathParam("id") int id) {

	}
 
}
