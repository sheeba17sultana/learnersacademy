package com.la.service.impl;

import java.util.List;

import com.la.DAO.TeacherDAO;
import com.la.DAO.impl.TeacherDAOImpl;
import com.la.service.TeacherService;
import com.model.pojo.Student;
import com.model.pojo.Subject;
import com.model.pojo.Teacher;

public class TeacherServiceImpl implements TeacherService{
	
	TeacherDAO dao = new TeacherDAOImpl();
	
	@Override
	public Teacher createTeacher(Teacher teacher) {
		// TODO Auto-generated method stub
		return dao.createTeacher(teacher);
	}

	@Override
	public Teacher getTeacherById(int id) {
		// TODO Auto-generated method stub
		return dao.getTeacherById(id);
	}

	@Override
	public List<Teacher> getAllTeacherList() {
		// TODO Auto-generated method stub
		return dao.getAllTeacherList();
	}

	@Override
	public void removeTeacher(int id) {
		// TODO Auto-generated method stub
		dao.removeTeacher(id);
	}

	@Override
	public Teacher updateTeacher(Teacher teacher) {
		// TODO Auto-generated method stub
		return dao.updateTeacher(teacher);
	}

	@Override
	public List<Subject> updateSubject(Teacher teacher) {
		// TODO Auto-generated method stub
		return dao.updateSubject(teacher);
	}

	@Override
	public List<Student> updateStudent(Teacher teacher) {
		// TODO Auto-generated method stub
		return  dao.updateStudent(teacher);
	}

	
	
}
