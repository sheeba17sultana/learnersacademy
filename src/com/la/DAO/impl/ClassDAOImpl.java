package com.la.DAO.impl;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.cfg.Configuration;

import com.la.DAO.ClassDAO;
import com.la.DAO.SubjectDAO;
import com.model.pojo.Classes;
import com.model.pojo.Student;
import com.model.pojo.Subject;
import com.model.pojo.Teacher;

public class ClassDAOImpl implements ClassDAO{
	Configuration configuration=new Configuration().configure();
	StandardServiceRegistryBuilder builder=new StandardServiceRegistryBuilder().applySettings(configuration.getProperties());
	SessionFactory factory=configuration.buildSessionFactory(builder.build());
	SubjectDAO dao = new SubjectDAOImpl();
	@Override
	public Classes createClasses(Classes classes) {
		Session session = factory.openSession();
		Transaction transaction = session.beginTransaction();
		session.persist(classes);
		transaction.commit();
		session.close();
		return classes;
	}
	@Override
	public Classes getClassById(int id) {
		Session session = factory.openSession();
		Transaction transaction = session.beginTransaction();
		Classes c = (Classes) session.get(Classes.class, id);
		session.save(c);
		transaction.commit();
		session.close();
		return c;
	}
	@Override
	public List<Classes> getAllClasses() {
		Session session = factory.openSession();
		Transaction transaction = session.beginTransaction();
		List<Classes> classList = session.createQuery("from com.model.pojo.Classes").list();
		transaction.commit();
		session.close();
		return classList;
	}
	@Override
	public void removeClass(int id) {
		Session session = factory.openSession();
		Transaction transaction = session.beginTransaction();
		Classes c = new Classes();
		c.setClassId(id);
		session.delete(c);
		transaction.commit();
		session.close();
	}
	@Override
	public Classes updateClass(Classes classes) {
		Session session = factory.openSession();
		Transaction transaction = session.beginTransaction();
		session.update(classes);
		transaction.commit();
		session.close();
		return classes;
	}
	@Override
	public List<Subject> updateSubject(Classes classes) {
		Session session = factory.openSession();
		Transaction transaction = session.beginTransaction();
		List<Subject> subjects = classes.getSubjectList();
		for (Subject subject : subjects) {
			Subject s1 = (Subject) session.get(Subject.class, subject.getSubjectId());
			s1.setClasses(classes);
			session.update(s1);
		}
		transaction.commit();
		session.close();
		return subjects;
	}
	
	@Override
	public Student getStudentId(int id) {
		Session session = factory.openSession();
		Transaction transaction = session.beginTransaction();
		Student st = (Student) session.get(Student.class, id);
		session.save(st);
		transaction.commit();
		session.close();
		return st;
	}


	@Override
	public List<Student> updateStudent(Classes classes) {
		Session session = factory.openSession();
		Transaction transaction = session.beginTransaction();
		List<Student> students = classes.getStudents();
		for (Student student : students) {
			Student s1 = (Student) session.get(Student.class, student.getStudentId());
			s1.setClasses(classes);
			session.update(s1);
		}
		transaction.commit();
		session.close();
		return students;
	}
	
	/*@Override
	public List<Student> getAllStudents() {
		Session session = factory.openSession();
		Transaction transaction = session.beginTransaction();
		List<Student> studentList = Student.class.getAllStudents();
		transaction.commit();
		session.close();
		return studentList;
	}*/
	@Override
	public Teacher getTeacherId(int id) {
		Session session = factory.openSession();
		Transaction transaction = session.beginTransaction();
		Teacher t = (Teacher) session.get(Teacher.class, id);
		session.save(t);
		transaction.commit();
		session.close();
		return t;
	}
	@Override
	public List<Subject> getAllSubjects() {
		Session session = factory.openSession();
		Transaction transaction = session.beginTransaction();
		List<Subject> subjectList = session.createQuery("from com.model.pojo.subject").list();
		transaction.commit();
		session.close();
		return subjectList;
	}
	@Override
	public List<Teacher> getAllTeacher(int id) {
		Session session = factory.openSession();
		Transaction transaction = session.beginTransaction();
		List<Teacher> teacherList = session.createQuery("from com.model.pojo.Teacher").list();
		transaction.commit();
		session.close();
		return teacherList;
	}
	@Override
	public Teacher updateTeacher(Teacher teacher) {
		Session session = factory.openSession();
		Transaction transaction = session.beginTransaction();
		session.update(teacher);
		transaction.commit();
		session.close();
		return teacher;
	}
	@Override
	public List<Student> getAllStudents(int id) {
		// TODO Auto-generated method stub
		//return null;
		Session session = factory.openSession();
		Transaction transaction = session.beginTransaction();
		List<Student> studentList = session.createQuery("from com.model.pojo.Student").list();
		transaction.commit();
		session.close();
		return studentList;
	}
}
