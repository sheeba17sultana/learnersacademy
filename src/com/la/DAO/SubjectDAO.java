package com.la.DAO;

import java.util.List;

import com.model.pojo.Subject;
import com.model.pojo.Teacher;

public interface SubjectDAO {
	public Subject createSubject(Subject subject);
	public Subject getSubjectById(int id);
	public List<Subject> getAllSubjectList();
	public void removeSubject(int id);
	public Subject updateSubject(Subject subject);
	public List<Subject> updateSubject(Teacher teacher);
}
