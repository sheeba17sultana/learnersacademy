package com.model.pojo;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.DynamicUpdate;


@Entity
@Table
@DynamicUpdate
public class Student{
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private int studentId;
	@Column(nullable = false)
	private String stuName;
	private String stuCourse;
	private long stuPhone;
	
	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "classId")	
	private Classes classes;
	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "teacherId")
	private Teacher teacher;
	public String getStuName() {
		return stuName;
	}
	public void setStuName(String stuName) {
		this.stuName = stuName;
	}
	public String getStuCourse() {
		return stuCourse;
	}
	public void setStuCourse(String stuCourse) {
		this.stuCourse = stuCourse;
	}
	public long getStuPhone() {
		return stuPhone;
	}
	public void setStuPhone(long stuPhone) {
		this.stuPhone = stuPhone;
	}
	
	public Student() {
		// Default constructor for Student class
		
	}
	public int getStudentId() {
		return studentId;
	}
	public void setStudentId(int studentId) {
		this.studentId = studentId;
	}
	
	public void setClasses(Classes classes) {
		this.classes = classes;
	}
	public void setTeacher(Teacher teacher) {
		this.teacher = teacher;
	}
	
	
}
